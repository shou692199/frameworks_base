/*
 * Copyright (C) 2023 ArrowOS
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.biometrics

import android.content.Context
import android.provider.Settings
import android.hardware.biometrics.common.AuthenticateReason
import javax.inject.Inject
import kotlinx.coroutines.flow.*

class FingerprintInteractiveToAuthProviderImpl @Inject constructor(
    private val mContext: Context
) : FingerprintInteractiveToAuthProvider {

    private val mDefaultValue: Int = if (mContext.resources.getBoolean(
            com.android.internal.R.bool.config_fingerprintWakeAndUnlock)
        ) 1 else 0

    override val enabledForCurrentUser: Flow<Boolean> = flow {
        var value = Settings.Secure.getInt(
            mContext.contentResolver,
            Settings.Secure.SFPS_PERFORMANT_AUTH_ENABLED, -1
        )
        if (value == -1) {
            value = mDefaultValue
            Settings.Secure.putInt(
                mContext.contentResolver,
                Settings.Secure.SFPS_PERFORMANT_AUTH_ENABLED, value
            )
        }
        emit(value == 0)
    }

    override fun getVendorExtension(userId: Int): AuthenticateReason.Vendor? = null
}
